<div align="center">
  <h1>🔋 Clean architecture with Nestjs</h1>
  <p>Next.js + TypeScript + Postgres starter packed useful for building efficient and scalable server-side applications.</p>
  <p>Made by <a href="https://maxwell-portfolio.vercel.app/">Maxwell Kimaiyo</a></p>
</div>

## Features

This repository is 🔋 battery packed with:

- ⚡️ Next.js
- ✨ TypeScript
- 🗄️ Postgres
- 🗂️ Redis - Act as temporary data store for high performance data access.
- 💀 Logger - For system logging 
- 🚬 Exception filters - To process all unhandled exceptions across the application
- 📗 OpenAPI (Swagger) - OpenAPI spec based on DTOs / entities using decorators
- ⚔️ Throttler - Rate-limiting to prevent applications from brute-force attacks
- 🛢️ Docker - To allow you to build, test, and deploy applications quickly
- 🃏 Jest — Configured for unit testing
- 📈 Absolute Import and Path Alias — Import components using `@/` prefix
- 📏 ESLint — Find and fix problems in your code, also will **auto sort** your imports
- 💖 Prettier — Format your code consistently
- 🐶 Husky & Lint Staged — Run scripts on your staged files before they are committed
- ⏰ Standard Version Changelog — Generate your changelog using `yarn release`
- 🔥 Architecture — Build using the best clean architecture to make the code robust and easy to maintain

## Getting Started

### 1. Clone this repository:

   ```bash
     git clone https://gitlab.com/Developerkimaiyo/clean-architecture-nest-js.git
   ```

### 2. Install dependencies

```bash
yarn install
```

### 3. Run the development server

You can start the server using this command:

```bash
yarn run start:dev
```
