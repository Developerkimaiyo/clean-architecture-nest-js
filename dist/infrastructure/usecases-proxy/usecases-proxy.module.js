"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var UsecasesProxyModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsecasesProxyModule = void 0;
const common_1 = require("@nestjs/common");
const repositories_module_1 = require("../repositories/repositories.module");
const usecases_proxy_1 = require("./usecases-proxy");
const contact_repository_1 = require("../repositories/contact.repository");
const getContacts_usecase_1 = require("../../usecases/contact/getContacts.usecase");
const addContact_usecase_1 = require("../../usecases/contact/addContact.usecase");
let UsecasesProxyModule = UsecasesProxyModule_1 = class UsecasesProxyModule {
    static register() {
        return {
            module: UsecasesProxyModule_1,
            providers: [
                {
                    inject: [contact_repository_1.DatabaseContactRepository],
                    provide: UsecasesProxyModule_1.GET_CONTACT_USECASES_PROXY,
                    useFactory: (contactRepository) => new usecases_proxy_1.UseCaseProxy(new getContacts_usecase_1.GetContactsUseCases(contactRepository)),
                },
                {
                    inject: [contact_repository_1.DatabaseContactRepository],
                    provide: UsecasesProxyModule_1.POST_CONTACT_USECASES_PROXY,
                    useFactory: (logger, contactRepository) => new usecases_proxy_1.UseCaseProxy(new addContact_usecase_1.AddContactUseCases(logger, contactRepository)),
                },
            ],
            exports: [
                UsecasesProxyModule_1.GET_CONTACT_USECASES_PROXY,
                UsecasesProxyModule_1.POST_CONTACT_USECASES_PROXY,
            ],
        };
    }
};
UsecasesProxyModule.GET_CONTACT_USECASES_PROXY = 'getContactsUsecasesProxy';
UsecasesProxyModule.POST_CONTACT_USECASES_PROXY = 'postContactUsecasesProxy';
UsecasesProxyModule = UsecasesProxyModule_1 = __decorate([
    (0, common_1.Module)({
        imports: [repositories_module_1.RepositoriesModule],
    })
], UsecasesProxyModule);
exports.UsecasesProxyModule = UsecasesProxyModule;
//# sourceMappingURL=usecases-proxy.module.js.map