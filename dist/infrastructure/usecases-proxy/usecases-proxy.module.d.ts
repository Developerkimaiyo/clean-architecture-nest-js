import { DynamicModule } from '@nestjs/common';
export declare class UsecasesProxyModule {
    static GET_CONTACT_USECASES_PROXY: string;
    static POST_CONTACT_USECASES_PROXY: string;
    static register(): DynamicModule;
}
