import { ContactModel } from '@/domain/model/contact';
export declare class ContactPresenter {
    id: number;
    email: string;
    message: string;
    createdDate: Date;
    updatedDate: Date;
    constructor(contact: ContactModel);
}
