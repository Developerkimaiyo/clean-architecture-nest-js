export declare class AddContactDto {
    readonly email: string;
    readonly message: string;
}
