"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const contacts_dto_1 = require("./contacts.dto");
const contact_presenter_1 = require("./contact.presenter");
const usecases_proxy_module_1 = require("../../../usecases-proxy/usecases-proxy.module");
const usecases_proxy_1 = require("../../../usecases-proxy/usecases-proxy");
const response_decorator_1 = require("../../../common/swagger/response.decorator");
const throttler_1 = require("@nestjs/throttler");
let ContactController = class ContactController {
    constructor(getContactsUsecaseProxy, addContactUsecaseProxy) {
        this.getContactsUsecaseProxy = getContactsUsecaseProxy;
        this.addContactUsecaseProxy = addContactUsecaseProxy;
    }
    async getContacts() {
        const contacts = await this.getContactsUsecaseProxy.getInstance().execute();
        return contacts.map((contact) => new contact_presenter_1.ContactPresenter(contact));
    }
    async addContact(addContactDto) {
        const { email, message } = addContactDto;
        const contactCreated = await this.addContactUsecaseProxy
            .getInstance()
            .execute(email, message);
        return new contact_presenter_1.ContactPresenter(contactCreated);
    }
};
__decorate([
    (0, common_1.Get)('contacts'),
    (0, response_decorator_1.ApiResponseType)(contact_presenter_1.ContactPresenter, true),
    (0, common_1.UseGuards)(throttler_1.ThrottlerGuard),
    (0, throttler_1.Throttle)(30, 60),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ContactController.prototype, "getContacts", null);
__decorate([
    (0, common_1.Post)('contact'),
    (0, response_decorator_1.ApiResponseType)(contact_presenter_1.ContactPresenter, true),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [contacts_dto_1.AddContactDto]),
    __metadata("design:returntype", Promise)
], ContactController.prototype, "addContact", null);
ContactController = __decorate([
    (0, common_1.Controller)('contact'),
    (0, swagger_1.ApiTags)('contact'),
    (0, swagger_1.ApiResponse)({ status: 500, description: 'Internal error' }),
    (0, swagger_1.ApiExtraModels)(contact_presenter_1.ContactPresenter),
    __param(0, (0, common_1.Inject)(usecases_proxy_module_1.UsecasesProxyModule.GET_CONTACT_USECASES_PROXY)),
    __param(1, (0, common_1.Inject)(usecases_proxy_module_1.UsecasesProxyModule.POST_CONTACT_USECASES_PROXY)),
    __metadata("design:paramtypes", [usecases_proxy_1.UseCaseProxy,
        usecases_proxy_1.UseCaseProxy])
], ContactController);
exports.ContactController = ContactController;
//# sourceMappingURL=contacts.controller.js.map