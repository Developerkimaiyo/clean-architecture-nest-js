import { GetContactsUseCases } from '@/usecases/contact/getContacts.usecase';
import { AddContactUseCases } from '@/usecases/contact/addContact.usecase';
import { AddContactDto } from '@/infrastructure/controllers/controllers/contact/contacts.dto';
import { ContactPresenter } from '@/infrastructure/controllers/controllers/contact/contact.presenter';
import { UseCaseProxy } from '@/infrastructure/usecases-proxy/usecases-proxy';
export declare class ContactController {
    private readonly getContactsUsecaseProxy;
    private readonly addContactUsecaseProxy;
    constructor(getContactsUsecaseProxy: UseCaseProxy<GetContactsUseCases>, addContactUsecaseProxy: UseCaseProxy<AddContactUseCases>);
    getContacts(): Promise<ContactPresenter[]>;
    addContact(addContactDto: AddContactDto): Promise<ContactPresenter>;
}
