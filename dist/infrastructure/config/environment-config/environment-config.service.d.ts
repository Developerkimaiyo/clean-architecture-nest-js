import { ConfigService } from '@nestjs/config';
import { DatabaseConfig } from '@/domain/config/database.interface';
export declare class EnvironmentConfigService implements DatabaseConfig {
    private configService;
    constructor(configService: ConfigService);
    getDatabaseHost(): string;
    getDatabasePort(): number;
    getDatabaseUser(): string;
    getDatabasePassword(): string;
    getDatabaseName(): string;
    getDatabaseSchema(): string;
    getDatabaseSync(): boolean;
    getLocalIP(): string;
    getRedisPort(): boolean;
}
