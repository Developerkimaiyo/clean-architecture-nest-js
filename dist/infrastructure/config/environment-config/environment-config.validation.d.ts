declare class EnvironmentVariables {
    DATABASE_HOST: string;
    DATABASE_PORT: number;
    DATABASE_USER: string;
    DATABASE_PASSWORD: string;
    DATABASE_NAME: string;
    DATABASE_SCHEMA: string;
    DATABASE_SYNCHRONIZE: boolean;
    LOCAL_IP: string;
    REDIS_PORT: string;
}
export declare function validate(config: Record<string, unknown>): EnvironmentVariables;
export {};
