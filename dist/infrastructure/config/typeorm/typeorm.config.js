"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const dotenv = require("dotenv");
dotenv.config({ path: './.env' });
const config = new typeorm_1.DataSource({
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    schema: process.env.DATABASE_SCHEMA,
    migrationsRun: true,
    entities: [__dirname + './../../**/*.entity{.ts,.js}'],
    migrationsTableName: 'migration-table',
    migrations: ['database/migrations/**/*{.ts,.js}'],
});
console.log(config);
exports.default = config;
//# sourceMappingURL=typeorm.config.js.map