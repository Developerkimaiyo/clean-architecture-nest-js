"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseContactRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const contact_1 = require("../../domain/model/contact");
const contact_entity_1 = require("../entities/contact.entity");
let DatabaseContactRepository = class DatabaseContactRepository {
    constructor(contactEntityRepository) {
        this.contactEntityRepository = contactEntityRepository;
    }
    async insert(contact) {
        const contactEntity = this.toContactEntity(contact);
        const result = await this.contactEntityRepository.insert(contactEntity);
        console.log(result);
        return this.toContact(result.generatedMaps[0]);
    }
    async findAll() {
        const contactsEntity = await this.contactEntityRepository.find();
        return contactsEntity.map((contactEntity) => this.toContact(contactEntity));
    }
    toContact(contactEntity) {
        const contact = new contact_1.ContactModel();
        contact.id = contactEntity.id;
        contact.email = contactEntity.email;
        contact.message = contactEntity.message;
        contact.createdDate = contactEntity.createdDate;
        contact.updatedDate = contactEntity.updatedDate;
        return contact;
    }
    toContactEntity(message) {
        const contactEntity = new contact_entity_1.Contact();
        contactEntity.id = message.id;
        contactEntity.email = message.email;
        contactEntity.message = message.message;
        return contactEntity;
    }
};
DatabaseContactRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(contact_entity_1.Contact)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], DatabaseContactRepository);
exports.DatabaseContactRepository = DatabaseContactRepository;
//# sourceMappingURL=contact.repository.js.map