import { Repository } from 'typeorm';
import { ContactModel } from '@/domain/model/contact';
import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { Contact } from '@/infrastructure/entities/contact.entity';
export declare class DatabaseContactRepository implements ContactRepository {
    private readonly contactEntityRepository;
    constructor(contactEntityRepository: Repository<Contact>);
    insert(contact: ContactModel): Promise<ContactModel>;
    findAll(): Promise<ContactModel[]>;
    private toContact;
    private toContactEntity;
}
