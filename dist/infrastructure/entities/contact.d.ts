export declare class Contact {
    id: number;
    email: string;
    message: string;
    createdDate: Date;
    updatedDate: Date;
}
