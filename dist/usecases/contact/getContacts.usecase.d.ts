import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { ContactModel } from '@/domain/model/contact';
export declare class GetContactsUseCases {
    private readonly contactRepository;
    constructor(contactRepository: ContactRepository);
    execute(): Promise<ContactModel[]>;
}
