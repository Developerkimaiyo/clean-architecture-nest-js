"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetContactsUseCases = void 0;
class GetContactsUseCases {
    constructor(contactRepository) {
        this.contactRepository = contactRepository;
    }
    async execute() {
        return await this.contactRepository.findAll();
    }
}
exports.GetContactsUseCases = GetContactsUseCases;
//# sourceMappingURL=getContacts.usecase.js.map