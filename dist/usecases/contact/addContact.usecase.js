"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddContactUseCases = void 0;
const contact_1 = require("../../domain/model/contact");
class AddContactUseCases {
    constructor(logger, contactRepository) {
        this.logger = logger;
        this.contactRepository = contactRepository;
    }
    async execute(email, message) {
        const contact = new contact_1.ContactModel();
        contact.email = email;
        contact.message = message;
        return await this.contactRepository.insert(contact);
    }
}
exports.AddContactUseCases = AddContactUseCases;
//# sourceMappingURL=addContact.usecase.js.map