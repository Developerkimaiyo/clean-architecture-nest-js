import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { ContactModel } from '@/domain/model/contact';
import { ILogger } from '@/domain/logger/logger.interface';
export declare class AddContactUseCases {
    private readonly logger;
    private contactRepository;
    constructor(logger: ILogger, contactRepository: ContactRepository);
    execute(email: string, message: string): Promise<ContactModel>;
}
