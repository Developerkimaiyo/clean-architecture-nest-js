import { ContactModel } from '../model/contact';
export interface ContactRepository {
    insert(contact: ContactModel): Promise<ContactModel>;
    findAll(): Promise<ContactModel[]>;
}
