export declare class ContactModel {
    id: number;
    email: string;
    message: string;
    createdDate: Date;
    updatedDate: Date;
}
