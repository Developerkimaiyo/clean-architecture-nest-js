"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrationTable1655733134496 = void 0;
class migrationTable1655733134496 {
    constructor() {
        this.name = 'migrationTable1655733134496';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "contact" ("id" SERIAL NOT NULL, "email" character varying(255) NOT NULL, "message" character varying(255), "create_date" TIMESTAMP NOT NULL DEFAULT now(), "updated_date" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_2cbbe00f59ab6b3bb5b8d19f989" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "contact"`);
    }
}
exports.migrationTable1655733134496 = migrationTable1655733134496;
//# sourceMappingURL=1655733134496-migration-table.js.map