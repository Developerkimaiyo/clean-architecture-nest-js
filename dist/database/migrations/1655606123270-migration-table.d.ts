import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class migrationTable1655606123270 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
