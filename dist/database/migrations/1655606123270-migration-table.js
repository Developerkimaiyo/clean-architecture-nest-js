"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrationTable1655606123270 = void 0;
class migrationTable1655606123270 {
    constructor() {
        this.name = 'migrationTable1655606123270';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`contact\` (\`id\` int NOT NULL AUTO_INCREMENT, \`email\` varchar(255) NOT NULL, \`message\` varchar(255) NULL, \`create_date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE \`contact\``);
    }
}
exports.migrationTable1655606123270 = migrationTable1655606123270;
//# sourceMappingURL=1655606123270-migration-table.js.map