import 'reflect-metadata';
import { DataSource } from 'typeorm';
import * as dotenv from 'dotenv';

dotenv.config({ path: './.env' });

const config = new DataSource({
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: parseInt(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  schema: process.env.DATABASE_SCHEMA,
  migrationsRun: true,
  entities: [__dirname + './../../**/*.entity{.ts,.js}'],
  migrationsTableName: 'migration-table',
  migrations: ['database/migrations/**/*{.ts,.js}'],
});

console.log(config);

export default config;
