import { DynamicModule, Module } from '@nestjs/common';

import { RepositoriesModule } from '../repositories/repositories.module';
import { UseCaseProxy } from './usecases-proxy';
import { DatabaseContactRepository } from '@/infrastructure/repositories/contact.repository';
import { GetContactsUseCases } from '@/usecases/contact/getContacts.usecase';
import { AddContactUseCases } from '@/usecases/contact/addContact.usecase';
import {LoggerService} from "@/infrastructure/logger/logger.service";

@Module({
  imports: [RepositoriesModule],
})
export class UsecasesProxyModule {
  static GET_CONTACT_USECASES_PROXY = 'getContactsUsecasesProxy';
  static POST_CONTACT_USECASES_PROXY = 'postContactUsecasesProxy';

  static register(): DynamicModule {
    return {
      module: UsecasesProxyModule,
      providers: [
        {
          inject: [DatabaseContactRepository],
          provide: UsecasesProxyModule.GET_CONTACT_USECASES_PROXY,
          useFactory: (contactRepository: DatabaseContactRepository) =>
            new UseCaseProxy(new GetContactsUseCases(contactRepository)),
        },
        {
          inject: [DatabaseContactRepository],
          provide: UsecasesProxyModule.POST_CONTACT_USECASES_PROXY,
          useFactory: (
            logger: LoggerService,
            contactRepository: DatabaseContactRepository,
          ) =>
            new UseCaseProxy(new AddContactUseCases(logger, contactRepository)),
        },
      ],
      exports: [
        UsecasesProxyModule.GET_CONTACT_USECASES_PROXY,
        UsecasesProxyModule.POST_CONTACT_USECASES_PROXY,
      ],
    };
  }
}
