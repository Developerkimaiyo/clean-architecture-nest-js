import { ApiProperty } from '@nestjs/swagger';
import { ContactModel } from '@/domain/model/contact';

export class ContactPresenter {
  @ApiProperty()
  id: number;
  @ApiProperty()
  email: string;
  @ApiProperty()
  message: string;
  @ApiProperty()
  createdDate: Date;
  @ApiProperty()
  updatedDate: Date;

  constructor(contact: ContactModel) {
    this.id = contact.id;
    this.email = contact.email;
    this.message = contact.message;
    this.createdDate = contact.createdDate;
    this.updatedDate = contact.updatedDate;
  }
}
