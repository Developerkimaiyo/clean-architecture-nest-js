import {Body, Controller, Get, Inject, Post, UseGuards} from '@nestjs/common';
import { ApiExtraModels, ApiResponse, ApiTags } from '@nestjs/swagger';

import { GetContactsUseCases } from '@/usecases/contact/getContacts.usecase';
import { AddContactUseCases } from '@/usecases/contact/addContact.usecase';
import { AddContactDto } from '@/infrastructure/controllers/controllers/contact/contacts.dto';
import { ContactPresenter } from '@/infrastructure/controllers/controllers/contact/contact.presenter';
import { UsecasesProxyModule } from '@/infrastructure/usecases-proxy/usecases-proxy.module';
import { UseCaseProxy } from '@/infrastructure/usecases-proxy/usecases-proxy';
import { ApiResponseType } from '@/infrastructure/common/swagger/response.decorator';
import {Throttle, ThrottlerGuard} from "@nestjs/throttler";

@Controller('contact')
@ApiTags('contact')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiExtraModels(ContactPresenter)
export class ContactController {
  constructor(
    @Inject(UsecasesProxyModule.GET_CONTACT_USECASES_PROXY)
    private readonly getContactsUsecaseProxy: UseCaseProxy<GetContactsUseCases>,
    @Inject(UsecasesProxyModule.POST_CONTACT_USECASES_PROXY)
    private readonly addContactUsecaseProxy: UseCaseProxy<AddContactUseCases>,
  ) {}

  @Get('contacts')
  @ApiResponseType(ContactPresenter, true)
  @UseGuards(ThrottlerGuard)
  @Throttle(30, 60)
  async getContacts() {
    const contacts = await this.getContactsUsecaseProxy.getInstance().execute();
    return contacts.map((contact) => new ContactPresenter(contact));
  }

  @Post('contact')
  @ApiResponseType(ContactPresenter, true)
  async addContact(@Body() addContactDto: AddContactDto) {
    const { email, message } = addContactDto;
    const contactCreated = await this.addContactUsecaseProxy
      .getInstance()
      .execute(email, message);
    return new ContactPresenter(contactCreated);
  }
}
