import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AddContactDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  readonly email: string;
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  readonly message: string;
}
