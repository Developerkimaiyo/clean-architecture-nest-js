import { Module } from '@nestjs/common';
import { UsecasesProxyModule } from '@/infrastructure/usecases-proxy/usecases-proxy.module';
import { ContactController } from '@/infrastructure/controllers/controllers/contact/contacts.controller';

@Module({
  imports: [UsecasesProxyModule.register()],
  controllers: [ContactController],
})
export class ControllersModule {}
