import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigModule } from '../config/typeorm/typeorm.module';

import { Contact } from '@/infrastructure/entities/contact.entity';
import { DatabaseContactRepository } from '@/infrastructure/repositories/contact.repository';

@Module({
  imports: [TypeOrmConfigModule, TypeOrmModule.forFeature([Contact])],
  providers: [DatabaseContactRepository],
  exports: [DatabaseContactRepository],
})
export class RepositoriesModule {}
