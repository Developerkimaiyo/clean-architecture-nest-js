import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContactModel } from '@/domain/model/contact';
import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { Contact } from '@/infrastructure/entities/contact.entity';

@Injectable()
export class DatabaseContactRepository implements ContactRepository {
  constructor(
    @InjectRepository(Contact)
    private readonly contactEntityRepository: Repository<Contact>,
  ) {}

  async insert(contact: ContactModel): Promise<ContactModel> {
    const contactEntity = this.toContactEntity(contact);
    const result = await this.contactEntityRepository.insert(contactEntity);
    console.log(result);
    return this.toContact(result.generatedMaps[0] as Contact);
  }

  async findAll(): Promise<ContactModel[]> {
    const contactsEntity = await this.contactEntityRepository.find();
    return contactsEntity.map((contactEntity) => this.toContact(contactEntity));
  }
  private toContact(contactEntity: Contact): ContactModel {
    const contact: ContactModel = new ContactModel();
    contact.id = contactEntity.id;
    contact.email = contactEntity.email;
    contact.message = contactEntity.message;
    contact.createdDate = contactEntity.createdDate;
    contact.updatedDate = contactEntity.updatedDate;
    return contact;
  }

  private toContactEntity(message: ContactModel): Contact {
    const contactEntity: Contact = new Contact();
    contactEntity.id = message.id;
    contactEntity.email = message.email;
    contactEntity.message = message.message;
    return contactEntity;
  }
}
