import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { ContactModel } from '@/domain/model/contact';

export class GetContactsUseCases {
  constructor(private readonly contactRepository: ContactRepository) {}

  async execute(): Promise<ContactModel[]> {
    return await this.contactRepository.findAll();
  }
}
