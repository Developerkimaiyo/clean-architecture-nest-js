import { ContactRepository } from '@/domain/repositories/contactRepository.interface';
import { ContactModel } from '@/domain/model/contact';
import { ILogger } from '@/domain/logger/logger.interface';

export class AddContactUseCases {
  constructor(
    private readonly logger: ILogger,
    private contactRepository: ContactRepository,
  ) {}

  async execute(email: string, message: string): Promise<ContactModel> {
    const contact = new ContactModel();
    contact.email = email;
    contact.message = message;
    return await this.contactRepository.insert(contact);
  }
}
