import { Module } from '@nestjs/common';

import { EnvironmentConfigModule } from '@/infrastructure/config/environment-config/environment-config.module';
import { RepositoriesModule } from '@/infrastructure/repositories/repositories.module';
import { UsecasesProxyModule } from '@/infrastructure/usecases-proxy/usecases-proxy.module';
import { ControllersModule } from '@/infrastructure/controllers/controllers/controllers.module';
import { LoggerModule } from './infrastructure/logger/logger.module';
import { ExceptionsModule } from './infrastructure/exceptions/exceptions.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    EnvironmentConfigModule,
    UsecasesProxyModule.register(),
    RepositoriesModule,
    ControllersModule,
    LoggerModule,
    ExceptionsModule,
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {}
